#基础镜像
FROM tomcat:latest

#作者
LABEL maintainer="jq <1404482005@qq.com>"

#运行安装telnet和nc
RUN apt-get install -y telnet nc; exit 0

#定义匿名卷 
VOLUME ["/home/jiangqian/tomcat"]

#TOMCAT环境变量
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH

#RUN mkdir -p "$CATALINA_HOME" 
#WORKDIR $CATALINA_HOME

#ENV CATALINA_BASE:   /usr/local/tomcat \
#    CATALINA_HOME:   /usr/local/tomcat \
#    CATALINA_TMPDIR: /usr/local/tomcat/temp \
#    JRE_HOME:        /usr

#启动入口
ENTRYPOINT ["catalina.sh","run"]



#拷贝war包到tomcat

#RUN cd $WORKSPACE

COPY target/hello-world-1.0-SNAPSHOT.war ${CATALINA_HOME}/webapps/

